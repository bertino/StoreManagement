package com.example.storemanagementapp.repository;

import com.example.storemanagementapp.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findRoleEntityByName(String roleName);
}
