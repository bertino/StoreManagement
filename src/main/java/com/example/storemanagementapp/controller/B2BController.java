package com.example.storemanagementapp.controller;

import com.example.storemanagementapp.model.entity.Product;
import com.example.storemanagementapp.model.entity.User;
import com.example.storemanagementapp.service.ProductService;
import com.example.storemanagementapp.service.SecurityService;
import com.example.storemanagementapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class B2BController {
    private final ProductService productService;
    private final UserService userService;
    private final SecurityService securityService;

    @GetMapping("/b2b")
    public String open(Model model, String error, String logout){
        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);

        Collection<Product> products = productService.listAllProductsByUsername(name);
        model.addAttribute("role", user.getRoles().iterator().next().getName());

        model.addAttribute("title", user.getRoles().iterator().next().getName());

        model.addAttribute("products", products);

        return "home";
    }

    @GetMapping("/add_product")
    public String addProduct(Model model) {
        model.addAttribute("product", new Product());
        return "add_product";
    }

    @PostMapping("/add_product/save")
    public String saveProduct(Product product, RedirectAttributes redirectAttributes) {
        productService.save(product);

        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);

        productService.addProductToUser(user.getEmail(), product);
        userService.updateUser(user);

        redirectAttributes.addFlashAttribute("message", "Product added succesfully");
        return "redirect:/";
    }

    @GetMapping("/edit{id}")
    public String editProduct(@PathVariable Long id, Model model) {
        model.addAttribute("product", productService.findById(id));
        return "edit_product";
    }

    @PostMapping("/edit_product")
    public String editProductSave(@ModelAttribute("product") final Product product, RedirectAttributes redirectAttributes) {
        Optional<Product> p = productService.findById(product.getId());
        //p.get().setName(product.getName());
        p.get().setQuantity(product.getQuantity());
        productService.save(p.get());
        return "redirect:/b2b";
    }
}
