package com.example.storemanagementapp.controller;

import com.example.storemanagementapp.model.entity.Product;
import com.example.storemanagementapp.model.entity.User;
import com.example.storemanagementapp.service.ProductService;
import com.example.storemanagementapp.service.SecurityService;
import com.example.storemanagementapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collection;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class ClientController {
    private final ProductService productService;
    private final UserService userService;
    private final SecurityService securityService;

    @GetMapping("/client")
    public String open(Model model){
        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);

        Collection<Product> products = productService.listAllProductsByB2C();

        model.addAttribute("role", user.getRoles().iterator().next().getName());
        model.addAttribute("products", products);
        model.addAttribute("title", user.getRoles().iterator().next().getName());

        return "home";
    }

    @GetMapping("add_to_cart{id}")
    public String addProductSave(@PathVariable("id") Long id) {
        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);

        Optional<Product> p = productService.findById(id);
        Product product = productService.findByName(p.get().getName());

        productService.addProductToUser(user.getEmail(), product);

        userService.updateUser(user);

        return "redirect:/";
    }

    @GetMapping("/cart")
    public String getCart(Model model){
        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);

        Collection<Product> products = productService.listAllProductsByUsername(name);
        model.addAttribute("role", user.getRoles().iterator().next().getName());

        model.addAttribute("products", products);

        return "home";
    }

    @GetMapping("/search{searchName}")
    public String search(Model model, @PathVariable("searchName") String searchName) {
        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);

        System.out.println(searchName);

        Collection<Product> products = productService.listAllProductsByUsername(searchName);
        model.addAttribute("role", user.getRoles().iterator().next().getName());

        model.addAttribute("products", products);

        return "redirect:/";
    }
}
