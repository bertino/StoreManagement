package com.example.storemanagementapp.controller;

import com.example.storemanagementapp.model.entity.Product;
import com.example.storemanagementapp.model.entity.User;
import com.example.storemanagementapp.service.ProductService;
import com.example.storemanagementapp.service.SecurityService;
import com.example.storemanagementapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collection;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class B2CController {
    private final ProductService productService;
    private final UserService userService;
    private final SecurityService securityService;

    @GetMapping("/b2c")
    public String open(Model model){
        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);

        Collection<Product> products = productService.listAllProductsByUsername(name);

        model.addAttribute("role", user.getRoles().iterator().next().getName());
        model.addAttribute("title", user.getRoles().iterator().next().getName());
        model.addAttribute("products", products);

        return "home";
    }

    @GetMapping("/buy_products")
    public String addProduct(Model model) {
        Collection<Product> products = productService.listAllProductsByB2B();
        model.addAttribute("products", products);
        return "buy_products";
    }

    @GetMapping("/buy_products/save{id}")
    public String addProductSave(@PathVariable("id") Long id) {
        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);

        Optional<Product> p = productService.findById(id);

        Product product = productService.findByName(p.get().getName());

        productService.addProductToUser(user.getEmail(), product);
        userService.updateUser(user);
        return "redirect:/";
    }
}
