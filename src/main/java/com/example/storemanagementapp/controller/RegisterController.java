package com.example.storemanagementapp.controller;

import com.example.storemanagementapp.model.entity.User;
import com.example.storemanagementapp.service.UserService;
import com.example.storemanagementapp.service.UserValidatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

@Controller
@RequestMapping("/register")
@RequiredArgsConstructor
public class RegisterController {
    private final UserValidatorService userValidatorService;
    private final UserService userService;

    @GetMapping()
    public String open(Model model){
        User user = new User();
        model.addAttribute("userForm", user);
        model.addAttribute("role", new String());

        return "register";
    }

    @PostMapping()
    public String register(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, @ModelAttribute("role") String role){
        //userValidatorService.validate(userForm, bindingResult);

        if (bindingResult.hasErrors())
            return "register";

        userService.save(userForm);

        userForm.setRoles(new ArrayList<>());

        if(role.equals("B2B")) {
            userService.addRoleToUser(userForm.getEmail(), "B2B");
        }

        if(role.equals("B2C")) {
            userService.addRoleToUser(userForm.getEmail(), "B2C");
        }

        if(role.equals("Client")) {
            userService.addRoleToUser(userForm.getEmail(), "Client");
        }

        userService.update(userForm);

        userService.login(userForm.getEmail(), userForm.getPassword());
        return "login";
    }
}
