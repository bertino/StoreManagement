package com.example.storemanagementapp.controller;

import com.example.storemanagementapp.model.entity.User;
import com.example.storemanagementapp.service.SecurityService;
import com.example.storemanagementapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class AdminController {
    private final SecurityService securityService;
    private final UserService userService;

    @GetMapping()
    public String open(Model model, String error, String logout){
        if (!securityService.isAuthenticated()) {
            return "login";
        }

        Collection<User> users = userService.listAllWithoutAdmin();

        model.addAttribute("users", users);

        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);
        model.addAttribute("name", name);

        if(!user.getIsActive()) {
            return "login";
        }

        if(user.getRoles().iterator().next().getName().equals("Admin")) {
            return "admin";
        }

        if(user.getRoles().iterator().next().getName().equals("B2B")) {
            return "redirect:/b2b";
        }

        if(user.getRoles().iterator().next().getName().equals("B2C")) {
            return "redirect:/b2c";
        }

        if(user.getRoles().iterator().next().getName().equals("Client")) {
            return "redirect:/client";
        }

        return "login";
    }

    @GetMapping("/admin/active{id}")
    public String makeActive(@PathVariable("id") Long id, Model model) {
        Optional<User> user = userService.findById(id);
        Collection<User> users = userService.listAllWithoutAdmin();
        user.get().setIsActive(true);
        userService.updateUser(user.get());
        model.addAttribute("users", users);
        return "admin";
    }

    @GetMapping("/admin/inactive{id}")
    public String makeInactive(@PathVariable("id") Long id, Model model) {
        Optional<User> user = userService.findById(id);
        Collection<User> users = userService.listAllWithoutAdmin();
        user.get().setIsActive(false);
        userService.updateUser(user.get());
        model.addAttribute("users", users);
        return "admin";
    }

    @GetMapping("/reset_password")
    public String resetPassword(Model model) {
        String name = securityService.authenticatedName();
        User user = userService.findByUsername(name);

        model.addAttribute("name", name);

        return "reset_password";
    }
}
