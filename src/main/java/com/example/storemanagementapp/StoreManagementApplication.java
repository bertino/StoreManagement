package com.example.storemanagementapp;

import com.example.storemanagementapp.model.entity.Role;
import com.example.storemanagementapp.service.RoleService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class StoreManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreManagementApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(RoleService roleService) {
        return args -> {
            roleService.saveRole(new Role(1L, "Admin"));
            roleService.saveRole(new Role(2L, "Client"));
            roleService.saveRole(new Role(3L, "B2B"));
            roleService.saveRole(new Role(4L, "B2C"));
        };
    }
}
