package com.example.storemanagementapp.service;

import com.example.storemanagementapp.model.entity.Product;
import com.example.storemanagementapp.model.entity.Role;
import com.example.storemanagementapp.model.entity.User;
import com.example.storemanagementapp.repository.ProductRepository;
import com.example.storemanagementapp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    public void save(Product product) {
        productRepository.save(product);
    }

    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    public Product findByName(String name) {
        return productRepository.findProductByName(name);
    }

    public List<Product> listAll() {
        List<Product> products = new ArrayList();
        productRepository.findAll().forEach((products::add));
        return products;
    }

    public void addProductToUser(String mail, Product product) {
        User user = userRepository.findByEmail(mail).orElse(null);
        assert user != null;
        user.getProducts().add(product);
    }

    public List<Product> listAllProductsByB2B() {
        List<Product> products = new ArrayList();
        List<User> users = new ArrayList();

        userRepository.findAll().forEach(user -> {
            if(user.getRoles().iterator().next().getName().equals("B2B")) {
                users.add(user);
            }
        });

        users.forEach(user -> {
            List<Product> list = (List<Product>)user.getProducts();

            list.forEach(p -> {
                products.add(p);
            });
        });

        return products;
    }

    public List<Product> listAllProductsByB2C() {
        List<Product> products = new ArrayList();
        List<User> users = new ArrayList();

        userRepository.findAll().forEach(user -> {
            if(user.getRoles().iterator().next().getName().equals("B2C")) {
                users.add(user);
            }
        });

        users.forEach(user -> {
            List<Product> list = (List<Product>)user.getProducts();

            list.forEach(product -> {
                products.add(product);
            });
        });

        return products;
    }

    public List<Product> listAllProductsByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return (List<Product>)user.getProducts();
    }
}
