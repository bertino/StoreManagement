package com.example.storemanagementapp.service;

import com.example.storemanagementapp.model.entity.Role;
import com.example.storemanagementapp.model.entity.User;
import com.example.storemanagementapp.repository.RoleRepository;
import com.example.storemanagementapp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> optUser = userRepository.findByEmail(email);

        if (optUser.isPresent()) {
            User appUser = optUser.get();

            return new org.springframework.security.core.userdetails.User(
                    appUser.getUsername(), appUser.getPassword(), true, true, true, true,
                    /* User Roles */
                    Objects.isNull(appUser.getRoles()) ?
                            new ArrayList(List.of(new SimpleGrantedAuthority("default")))
                            : appUser.getRoles()
                                .stream()
                                .map(Role::getName)
                                .map(SimpleGrantedAuthority::new)
                                .toList()
            );
        }
        throw new UsernameNotFoundException(email);
    }

    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public User update(User user) {
        return userRepository.save(user);
    }

    public void login(String email, String password){
        UserDetails userDetails = this.loadUserByUsername(email);

        if(Objects.isNull(userDetails))
            return;

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    public List<User> listAll(){
        List<User> users = new ArrayList();
        userRepository.findAll().forEach((users::add));
        return users;
    }

    public List<User> listAllWithoutAdmin(){
        List<User> users = new ArrayList();
        userRepository.findAll().forEach(user -> {
            if(!user.getRoles().iterator().next().getName().equals("Admin")) {
                users.add(user);
            }
        });
        return users;
    }



    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public void addRoleToUser(String mail, String roleName) {
        User user = userRepository.findByEmail(mail).orElse(null);
        Role role = roleRepository.findRoleEntityByName(roleName);
        user.getRoles().add(role);
    }

    public List<User> listAllB2C(){
        List<User> users = new ArrayList();
        userRepository.findAll().forEach(user -> {
            if(user.getRoles().iterator().next().getName().equals("B2C")) {
                users.add(user);
            }
        });
        return users;
    }
}
